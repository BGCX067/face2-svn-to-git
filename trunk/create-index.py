#-*- coding:utf-8 -*-
#   Programmer:     sunjoy
#   E-mail:         sunjunyi@software.ict.ac.cn
#
#   Copyleft 2007 sunjoy
#
#   Distributed under the terms of the GPL (GNU Public License)
#
#   FACE is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#   version2.1

import cPickle,os
from facecore import *

BASE_DIR = os.path.dirname(__file__)
if BASE_DIR == "" : BASE_DIR = "."

C2P_FILE = BASE_DIR + "/db/c2p.db" #dictionary of chinese character to pinyin
WORD_FILE = BASE_DIR + "/prepare/words.txt" #file of words,one utf-8 word per line

def mul(B,i):
    if i == (len(B)-1):
        return B[i]
    else:
        BB = mul(B,i+1)
        result = []
        for c in B[i]:
            for x in BB:
                result.append(c+x)
        return result

def pinyin(w):
    global p2c,c2p
    buf=[ []  for i in xrange(len(w))]
    for i in xrange(len(w)):
        c = w[i]
        if c2p.has_key(c):
            for pin in c2p[c]:
                buf[i].append(pin)
            flag = True
        else:
            buf[i].append(c)
    pincodes = mul(buf,0)
    return pincodes

if __name__ == "__main__":
    print "Initializ....."
    core = FaceCore()
    p2c = cPickle.load(file(FaceCore.P2C_FILE,'rb'))
    c2p = cPickle.load(file(C2P_FILE,'rb'))
    f = open(WORD_FILE,"r")
    print "Processing...."
    while True:
        try:
            line = f.readline()
            if line=="":
                break
            line = line.strip()
            if line=="":
                continue
            word = line.decode('utf-8')
            core.addWord(word)
            if len(word)<10:
                pys = pinyin(word)
                for p in pys:
                    if p<>word:
                        core.addWord(p)
                        p2c[p.lower()] = word.lower()
        except  Exception, e:
            print e
    core.save()
    cPickle.dump(p2c,file(FaceCore.P2C_FILE,'wb'))
    print "Done."