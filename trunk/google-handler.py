import wsgiref.handlers,os,cPickle

from google.appengine.ext import webapp
from facecore import FaceCore

class MainPage(webapp.RequestHandler):
  def get(self):
    self.response.headers['Content-Type'] = 'text/plain'
    self.response.out.write('Hello, webapp World!')

class SearchPage(webapp.RequestHandler):
  global core

  def query(self,keyword):
    try:
      ukey = keyword.decode('utf-8')
    except:
      ukey = keyword.decode('gbk')
    result =[]
    got = core.query(ukey)
    for g in got:
      result.append('"'+g.encode('utf-8')+'"')
    return "["+",".join(result)+"]"

  def get(self):
    self.response.headers['Content-Type'] = 'text/plain; charset=UTF-8'
    keyword = self.request.get("q")
    if (keyword<>None) and (keyword<>""):
      result = self.query(keyword)
      self.response.out.write(result)

def main():
  application = webapp.WSGIApplication(
                                       [('/', MainPage),('/s',SearchPage)],
                                       debug=True)
  wsgiref.handlers.CGIHandler().run(application)

if __name__ == "__main__":
  core = FaceCore()
  main()

